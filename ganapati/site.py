from flask import Blueprint

bp = Blueprint('auth', __name__, url_prefix='/')


@bp.route('')
def index():
    return 'Hello Index Test from RR301-B'
