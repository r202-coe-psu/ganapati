FROM debian:sid
RUN apt update && apt upgrade -y
RUN apt install -y python3 python3-dev python3-pip python3-venv

COPY . /app
WORKDIR /app

RUN pip3 install flask
RUN python3.7 setup.py develop

#ENV FLASK_ENV=prodoction
#ENV AUTHLIB_INSECURE_TRANSPORT=true


#EXPOSE 8080
#ENTRYPOINT ['sadhu-web']
