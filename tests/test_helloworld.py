import unittest

import ganapati


# see more https://flask.palletsprojects.com/en/1.1.x/testing/

class BasicTests(unittest.TestCase):

    def setUp(self):
        app = ganapati.create_app()
        app.config['TESTING'] = True

        self.client = app.test_client()
        self.app = app

    def tearDown(self):
        pass

    def test_get_hello_got_helloworld(self):
        response = self.client.get('/hello', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        data = response.data.decode()
        self.assertIn('Hello, World!', data)


if __name__ == "__main__":
    unittest.main()
